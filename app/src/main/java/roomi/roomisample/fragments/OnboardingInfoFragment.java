package roomi.roomisample.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;

import java.util.ArrayList;
import java.util.List;

import roomi.roomisample.R;
import roomi.roomisample.databinding.FragmentOnboardingInfoBinding;
import roomi.roomisample.events.SignUpEvent;
import roomi.roomisample.utils.CustomAnimations;
import roomi.roomisample.utils.CustomClickableSpan;

public class OnboardingInfoFragment extends BaseFragment {

    private static final String TAG = OnboardingInfoFragment.class.getSimpleName();
    private int mScreenHeight;
    private int mScreenWidth;


    private List<View> mViewsToAnimateInAndOut = new ArrayList<>();


    private FragmentOnboardingInfoBinding b;

    public static OnboardingInfoFragment newInstance() {

        Bundle args = new Bundle();

        OnboardingInfoFragment fragment = new OnboardingInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        mScreenHeight = metrics.heightPixels;
        mScreenWidth = metrics.widthPixels;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_onboarding_info, container, false);

        mViewsToAnimateInAndOut.add(b.onboardingInfoLogoImageView);
        mViewsToAnimateInAndOut.add(b.onboardingInfoSloganTextView);
        mViewsToAnimateInAndOut.add(b.onboardingInfoFacebookButton);
        mViewsToAnimateInAndOut.add(b.onboardingInfoLoginOrSignupTextView);

        b.onboardingInfoLogoImageView.setAlpha(0f);


        Spannable sb = Spannable.Factory.getInstance().newSpannable("Or Sign up or Login with email");
//        final SpannableStringBuilder sb = new SpannableStringBuilder("your text here");


        CustomClickableSpan loginClickableSpan = new CustomClickableSpan(widget -> {

            Log.d(TAG, "LOGIN");

        });

        CustomClickableSpan signUpClickableSpan = new CustomClickableSpan(widget -> {

            Log.d(TAG, "SIGNUP");
            CustomAnimations customAnimations = new CustomAnimations(mViewsToAnimateInAndOut);
            customAnimations.cascadeViewsOut(() -> {

                mBus.post(new SignUpEvent());

            });

        });




        sb.setSpan(signUpClickableSpan, 3, 10, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        sb.setSpan(loginClickableSpan, 14, 19, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        b.onboardingInfoLoginOrSignupTextView.setText(sb);
        b.onboardingInfoLoginOrSignupTextView.setMovementMethod(LinkMovementMethod.getInstance());

        b.onboardingInfoLogoImageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            public void onGlobalLayout() {
                b.onboardingInfoLogoImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                float x = b.onboardingInfoLogoImageView.getX();
                float y = b.onboardingInfoLogoImageView.getY();

                b.onboardingInfoMagGlassImageView.setX(x);
                b.onboardingInfoMagGlassImageView.setY(mScreenHeight);

                b.onboardingInfoMagGlassImageView
                        .animate()
                        .setStartDelay(500)
                        .setDuration(1500)
                        .setInterpolator(new DecelerateInterpolator())
                        .x(x)
                        .y(y)
                        .setListener(afterGlassSwoopsInListener)
                        .start();

            }
        });




        return b.getRoot();
    }

    private final AnimatorListenerAdapter afterGlassSwoopsInListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            b.onboardingInfoLogoImageView
                    .animate()
                    .setDuration(1000)
                    .alpha(1f)
                    .setListener(afterLogoFadesInListener)
                    .start();
        }
    };

    private final AnimatorListenerAdapter afterLogoFadesInListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);

            b.onboardingInfoMagGlassImageView.setVisibility(View.GONE);

            b.onboardingInfoOrangeOverlay
                    .animate()
                    .alpha(0f)
                    .setDuration(1000)
                    .start();

        }
    };


}
