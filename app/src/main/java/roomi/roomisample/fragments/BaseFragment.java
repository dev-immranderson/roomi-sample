package roomi.roomisample.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.squareup.otto.Bus;

import roomi.roomisample.utils.BusProvider;

public class BaseFragment extends Fragment {

    protected final Bus mBus = BusProvider.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBus.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBus.unregister(this);
    }
}
