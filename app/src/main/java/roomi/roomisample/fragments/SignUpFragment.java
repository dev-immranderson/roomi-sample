package roomi.roomisample.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import roomi.roomisample.R;
import roomi.roomisample.databinding.FragmentSignUpBinding;

public class SignUpFragment extends BaseFragment {


    private FragmentSignUpBinding b;

    public static SignUpFragment newInstance() {

        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        b = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);
        b.signUpFab.hide();

        b.signUpFab.setOnClickListener(v -> {


        });

        b.signUpPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 7) {

                    b.signUpFab.show();

                } else {

                    b.signUpFab.hide();

                }

            }
        });

        return b.getRoot();

    }
}
