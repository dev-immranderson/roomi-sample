package roomi.roomisample.interfaces;

public interface CustomAnimationEndListener {

    void onCustomAnimationEnded();

}
