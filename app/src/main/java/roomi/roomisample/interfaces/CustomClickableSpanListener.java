package roomi.roomisample.interfaces;

import android.view.View;

public interface CustomClickableSpanListener {

    void onCustomClick(View widget);

}
