package roomi.roomisample.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.squareup.otto.Bus;

import roomi.roomisample.utils.BusProvider;

public class BaseActivity extends AppCompatActivity {

    protected final Bus mBus = BusProvider.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBus.register(this);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBus.unregister(this);
    }


}
