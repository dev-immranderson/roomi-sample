package roomi.roomisample.activities;

import android.databinding.DataBindingUtil;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.TextureView;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import java.io.IOException;

import roomi.roomisample.R;
import roomi.roomisample.databinding.ActivityOnboardingBinding;
import roomi.roomisample.events.LoginEvent;
import roomi.roomisample.events.SignUpEvent;
import roomi.roomisample.fragments.OnboardingInfoFragment;
import roomi.roomisample.fragments.SignUpFragment;

import static roomi.roomisample.constants.LoginTypes.LoginType.EMAIL;

public class OnboardingActivity extends BaseActivity {

    private static final String TAG = OnboardingActivity.class.getSimpleName();
    private ActivityOnboardingBinding b;

    private static final String BACKGROUND_VIDEO_URL = "http://roomiapp.com/video.mp4";
    private MediaPlayer mMediaPlayer;
    private int mScreenHeight;
    private int mScreenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        mScreenHeight = metrics.heightPixels;
        mScreenWidth = metrics.widthPixels;

        b = DataBindingUtil.setContentView(this, R.layout.activity_onboarding);

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.onboarding_content_fl, OnboardingInfoFragment.newInstance())
                    .commit();

        }

        b.onboardingBackgroundTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, int width, int height) {

                Surface surface = new Surface(surfaceTexture);

                try {

                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setDataSource(BACKGROUND_VIDEO_URL);
                    mMediaPlayer.setSurface(surface);
                    mMediaPlayer.setLooping(true);
                    mMediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

                    mMediaPlayer.prepareAsync();

                    mMediaPlayer.setOnPreparedListener(mp -> {

                        int finalVideoHeight = mScreenHeight;
                        int finalVideoWidth = (mp.getVideoWidth() * mScreenHeight) / mp.getVideoHeight();


                        ViewGroup.LayoutParams layoutParams = b.onboardingBackgroundTextureView.getLayoutParams();
                        layoutParams.width = finalVideoWidth;
                        layoutParams.height = finalVideoHeight;

                        int calculatedOffset = (finalVideoWidth - mScreenWidth) / 2;

                        b.onboardingBackgroundTextureView.setLayoutParams(layoutParams);
                        b.onboardingBackgroundTextureView.setX(-calculatedOffset);

                        mp.start();
                    });

                } catch (IllegalArgumentException | SecurityException | IOException | IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });

    }

    @Subscribe
    public void onLoginEventReceived(LoginEvent loginEvent) {


        switch (loginEvent.loginType) {

            case EMAIL:



                break;


        }
    }

    @Subscribe
    public void onSignUpEventReceived(SignUpEvent signUpEvent) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.onboarding_content_fl, SignUpFragment.newInstance())
                .addToBackStack(null)
                .commit();

    }

}
