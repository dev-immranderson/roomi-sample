package roomi.roomisample.constants;

import android.support.annotation.IntDef;

public class LoginTypes {

    @IntDef({
            LoginType.EMAIL,
            LoginType.FACEBOOK
    })

    public @interface LoginType {

        int EMAIL = 100;
        int FACEBOOK = 101;

    }

}
