package roomi.roomisample.utils;

import android.app.Application;

import com.facebook.FacebookSdk;

public class RoomiApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
    }
}
