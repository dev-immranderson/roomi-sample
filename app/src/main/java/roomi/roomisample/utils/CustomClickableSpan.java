package roomi.roomisample.utils;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import roomi.roomisample.interfaces.CustomClickableSpanListener;

public class CustomClickableSpan extends ClickableSpan {

    private final CustomClickableSpanListener mCallback;

    public CustomClickableSpan(CustomClickableSpanListener callback) {

        mCallback = callback;

    }

    @Override
    public void onClick(View widget) {

        mCallback.onCustomClick(widget);

    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(false);
        ds.setColor(Color.parseColor("#B7450A"));

    }
}