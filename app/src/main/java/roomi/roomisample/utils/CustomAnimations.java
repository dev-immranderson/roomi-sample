package roomi.roomisample.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

import java.util.List;

import roomi.roomisample.interfaces.CustomAnimationEndListener;

public class CustomAnimations {

    private List<View> mViews;
    private CustomAnimationEndListener mListener;

    public CustomAnimations(List<View> views) {

        mViews = views;

    }

    public void cascadeViewsOut(CustomAnimationEndListener listener) {

        mListener = listener;
        cascadeNextView(0);

    }

    private void cascadeNextView(int pos) {

        if (pos < mViews.size()) {

            View view = mViews.get(pos);

            if (pos != mViews.size() - 1) {

                view.animate()
                        .x(0)
                        .alpha(0f)
                        .setDuration(750)
                        .setStartDelay(150)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                super.onAnimationStart(animation);
                                cascadeNextView(pos + 1);
                            }
                        })
                        .start();

            } else {

                view.animate()
                        .x(0)
                        .alpha(0f)
                        .setDuration(750)
                        .setStartDelay(150)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                mListener.onCustomAnimationEnded();
                            }
                        })
                        .start();

            }
        }


    }
}
